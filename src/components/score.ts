export class Note {
  
  private x: number;
  private y: number;
  private staff: number;
  private note_type: NoteType;
  private pitch: number;
  private accidental: number;

  constructor(
    x: number = 10,
    y: number = 10,
    staff: number = 0,
    note_type: NoteType = NoteType.Quarter,
    pitch: number = 0,
    accidental: number = 0
  ) {
    this.x = x;
    this.y = y;
    this.staff = staff;
    this.note_type = note_type;
    this.pitch = pitch;
    this.accidental = accidental;
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.save();
    //ctx.translate(this.x, this.y);

    switch (this.note_type) {
      case NoteType.Whole:
        this.drawWholeNote(ctx);
        break;
      case NoteType.Quarter:
        this.drawQuarterNote(ctx);
        break;
      case NoteType.Eighth:
        this.drawEighthNote(ctx);
        break;
      case NoteType.Sixteenth:
        this.drawSixteenthNote(ctx);
        break;
    }
    ctx.restore();
  }

  private drawWholeNote(ctx: CanvasRenderingContext2D) {
    ctx.beginPath();
    ctx.arc(50, 50 + 100 * this.staff * 5, 40, 0, 2 * Math.PI);
    ctx.fill();
  }

  private drawQuarterNote(ctx: CanvasRenderingContext2D) {
    ctx.beginPath();
    ctx.arc(50, 50 + 100 * this.staff * 5, 40, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(50, 50 + 100 * this.staff * 5);
    ctx.lineTo(50, 10 + 100 * this.staff * 5);
    ctx.stroke();
  }

  private drawEighthNote(ctx: CanvasRenderingContext2D) {
    ctx.beginPath();
    ctx.arc(50, 50 + 100 * this.staff * 5, 40, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(50, 50 + 100 * this.staff * 5);
    ctx.lineTo(50, 10 + 100 * this.staff * 5);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(50, 10 + 100 * this.staff * 5, 5, 0, 2 * Math.PI);
    ctx.fill();
  }
  private drawSixteenthNote(ctx: CanvasRenderingContext2D) {
    ctx.beginPath();
    ctx.arc(50, 50 + 100 * this.staff * 5, 40, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(50, 50 + 100 * this.staff * 5);
    ctx.lineTo(50, 10 + 100 * this.staff * 5);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(50, 10 + 100 * this.staff * 5, 5, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(50, 10 + 100 * this.staff * 5);
    ctx.lineTo(60, 20 + 100 * this.staff * 5);
    ctx.stroke();
  }

}

export enum NoteType {
  Whole,
  Quarter,
  Eighth,
  Sixteenth,
}


export class Score {
  private notes: Note[];

  constructor() {
    this.notes = [];
  }

  addNote(note: Note) {
    this.notes.push(note);
  }

  removeNote(note: Note) {
    const index = this.notes.indexOf(note);
    if (index >= 0) {
      this.notes.splice(index, 1);
    }
  }

  clear() {
    this.notes = [];
  }

  /*This method first saves the current canvas state, then translates and scales the canvas so that the score is drawn with the correct orientation. 
    It then draws the score lines and the notes. The notes are drawn using the draw method of the Note class, which will need to be implemented separately.*/
  draw(ctx: CanvasRenderingContext2D, lines: number) {
    ctx.save();
    ctx.translate(0, lines * 10);
    ctx.scale(1, -1);

    // draw the score lines
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    for (let i = 0; i < lines; i++) {
      ctx.beginPath();
      ctx.moveTo(0, i * 10);
      ctx.lineTo(500, i * 10);
      ctx.stroke();
    }

    // draw the notes
    for (const note of this.notes) {
      note.draw(ctx);
    }

    ctx.restore();
  }
}
